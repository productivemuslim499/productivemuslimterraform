resource "aws_vpc" "main" {
  cidr_block           = "10.1.0.0/16"
  enable_dns_support   = true
  enable_dns_hostnames = true

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-vpc" })
  )
}

resource "aws_internet_gateway" "main" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-internet-gateway" })
  )
}

##################################################
# Public Subnets - Inbound/Outbound Internet Access
##################################################

# resource "aws_subnet" "public-a" {
#   cidr_block              = "10.1.1.0/24"
#   map_public_ip_on_launch = true
#   vpc_id                  = aws_vpc.main.id
#   availability_zone       = "${data.aws_region.current.name}a"

#   tags = merge(
#     local.common-tags,
#     tomap({ "Name" = "${local.prefix}-public-subnet-a" })
#   )
# }

# resource "aws_subnet" "public-b" {
#   cidr_block              = "10.1.2.0/24"
#   map_public_ip_on_launch = true
#   vpc_id                  = aws_vpc.main.id
#   availability_zone       = "${data.aws_region.current.name}b"

#   tags = merge(
#     local.common-tags,
#     tomap({ "Name" = "${local.prefix}-public-subnet-b" })
#   )
# }

resource "aws_subnet" "public-subnet" {
  count                   = var.public-subnets-count
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.cidrs[count.index]
  map_public_ip_on_launch = false
  availability_zone       = var.az-list[count.index]

  tags = {
    Name = "public-subnet-${count.index + 1}"
  }
}

resource "aws_route_table" "public-a-rt" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-public-route-a" })
  )
}

resource "aws_route_table" "public-b-rt" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-public-route-b" })
  )
}

resource "aws_route_table_association" "public-rt-assoc" {
  count          = var.public-subnets-count
  subnet_id      = aws_subnet.public-subnet[count.index].id
  route_table_id = aws_route_table.public-a-rt.id
}

# resource "aws_route_table_association" "public-b-rt-assoc" {
#   subnet_id      = aws_subnet.public-b.id
#   route_table_id = aws_route_table.public-b-rt.id
# }

resource "aws_route" "public-internet-access-a" {
  route_table_id         = aws_route_table.public-a-rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

resource "aws_route" "public-internet-access-b" {
  route_table_id         = aws_route_table.public-b-rt.id
  destination_cidr_block = "0.0.0.0/0"
  gateway_id             = aws_internet_gateway.main.id
}

resource "aws_eip" "public-a-eip" {
  vpc = true

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-public-eip-a" })
  )
}


resource "aws_eip" "public-b-eip" {
  vpc = true

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-public-eip-b" })
  )
}

resource "aws_nat_gateway" "public-gateway-a" {
  allocation_id = aws_eip.public-a-eip.id
  subnet_id     = aws_subnet.public-subnet[0].id

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-public-nat-a" })
  )
}

resource "aws_nat_gateway" "public-gateway-b" {
  allocation_id = aws_eip.public-b-eip.id
  subnet_id     = aws_subnet.public-subnet[1].id

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-public-nat-b" })
  )
}

##################################################
# Private Subnets - Inbound/Outbound Internet Access
##################################################
# resource "aws_subnet" "private-subnet-a" {
#   cidr_block        = var.cidrs[2]
#   vpc_id            = aws_vpc.main.id
#   availability_zone = "${data.aws_region.current.name}a"

#   tags = merge(
#     local.common-tags,
#     tomap({ "Name" = "${local.prefix}-private-a" })
#   )
# }

# resource "aws_subnet" "private-subnet-b" {
#   cidr_block        = var.cidrs[3]
#   vpc_id            = aws_vpc.main.id
#   availability_zone = "${data.aws_region.current.name}a"

#   tags = merge(
#     local.common-tags,
#     tomap({ "Name" = "${local.prefix}-private-b" })
#   )
# }

resource "aws_subnet" "private-subnet" {
  count                   = var.private-subnets-count
  vpc_id                  = aws_vpc.main.id
  cidr_block              = var.private-cidrs[count.index]
  map_public_ip_on_launch = false
  availability_zone       = var.az-list[count.index]

  tags = {
    Name = "public-subnet-${count.index + 1}"
  }
}

resource "aws_route_table" "private-rt-a" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-private-rt-a" })
  )
}
resource "aws_route_table" "private-rt-b" {
  vpc_id = aws_vpc.main.id

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-private-rt-b" })
  )
}

resource "aws_route_table_association" "private-rt-assoc-a" {
  subnet_id      = aws_subnet.private-subnet[0].id
  route_table_id = aws_route_table.private-rt-a.id
}

resource "aws_route_table_association" "private-rt-assoc-b" {
  subnet_id      = aws_subnet.private-subnet[1].id
  route_table_id = aws_route_table.private-rt-b.id
}

resource "aws_route" "private_a_internet_out" {
  route_table_id         = aws_route_table.private-rt-a.id
  nat_gateway_id         = aws_nat_gateway.public-gateway-a.id
  destination_cidr_block = "0.0.0.0/0"
}

resource "aws_route" "private_b_internet_out" {
  route_table_id         = aws_route_table.private-rt-b.id
  nat_gateway_id         = aws_nat_gateway.public-gateway-b.id
  destination_cidr_block = "0.0.0.0/0"
}

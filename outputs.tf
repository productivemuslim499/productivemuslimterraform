output "db-host" {
  value = aws_db_instance.main-db.address
}

output "bastion-host" {
  value = aws_instance.bastion.public_dns
}

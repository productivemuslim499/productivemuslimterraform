resource "aws_ecs_cluster" "main-cluster" {
  name = "${local.prefix}-cluster"

  tags = local.common-tags
}

# IAM policy for task execution
resource "aws_iam_policy" "task-execution-role-policy" {
  name        = "${local.prefix}-task-exec-role-policy"
  path        = "/"
  description = "Allow retrieving of images and adding to logs"
  policy      = file("./templates/task-exec-role.json")
}

# Role to assume task execution
resource "aws_iam_role" "task-execution-role" {
  name               = "${local.prefix}-task-exec-role-policy"
  assume_role_policy = file("./templates/ecs-assume-role-policy.json")

  tags = local.common-tags
}

# policy role attachment
resource "aws_iam_role_policy_attachment" "task-execution-role" {
  role       = aws_iam_role.task-execution-role.name
  policy_arn = aws_iam_policy.task-execution-role-policy.arn
}

# Runtime permissions to task
resource "aws_iam_role" "app-iam-role" {
  name               = "${local.prefix}-api-task"
  assume_role_policy = file("./templates/ecs-assume-role-policy.json")

  tags = local.common-tags
}

# Log Group
resource "aws_cloudwatch_log_group" "ecs-task-logs" {
  name = "${local.prefix}-api-logs"

  tags = local.common-tags
}

data "template_file" "api-container-definitions" {
  template = file("./templates/container-definitions.json")

  vars = {
    app_image         = var.ecr-image-api
    proxy_image       = var.ecr-image-proxy
    django_secret_key = var.django-secret-key
    db_host           = aws_db_instance.main-db.address
    db_name           = aws_db_instance.main-db.name
    db_user           = aws_db_instance.main-db.username
    db_pass           = aws_db_instance.main-db.password
    log_group_name    = aws_cloudwatch_log_group.ecs-task-logs.name
    log_group_region  = var.aws-region
    allowed_hosts     = "*" // all hosts allowed for testing.  use loadbalancer to limit hosts
  }
}


resource "aws_ecs_task_definition" "api" {
  family                   = "${local.prefix}-api"
  container_definitions    = data.template_file.api-container-definitions.rendered
  requires_compatibilities = ["FARGATE"] # serverless container management
  network_mode             = "awsvpc"    # allows images access to resources in vpc such as DB
  cpu                      = 256
  memory                   = 512
  execution_role_arn       = aws_iam_role.task-execution-role.arn
  task_role_arn            = aws_iam_role.app-iam-role.arn # running task permissions

  volume {
    name = "static"
  }

  tags = local.common-tags
}

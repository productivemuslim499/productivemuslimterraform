variable "prefix" {
  default = "pm"
}

variable "project" {
  default = "Productive-Muslim"
}

variable "contact" {
  default = "productivemuslim99@gmail.com"
}

variable "cidrs" {
  type = list(string)
}

variable "private-cidrs" {
  type = list(string)
}

variable "aws-region" {}

variable "public-subnets-count" {
  type    = number
  default = 2
}

variable "db-username" {
  description = "Username for RDS postgres DB"
}

variable "db-password" {
  description = "Password for RDS postgres DB"
}

variable "private-subnets-count" {
  type    = number
  default = 2
}

variable "az-list" {
  type    = list(string)
  default = ["us-east-1a", "us-east-1b"]
}

variable "key-name" {
  default = "bastion-keyname"
}

variable "ecr-image-api" {
  description = "ECR image for API"
  default     = "157150173222.dkr.ecr.us-east-1.amazonaws.com/nginx-app-proxy:latest" //ECR image url
}

variable "ecr-image-proxy" {
  description = "ECR image proxy"
  default     = "157150173222.dkr.ecr.us-east-1.amazonaws.com/nginx-app-proxy:latest"
}

variable "django-secret-key" {
  description = "Secret Key for Django App"
  default     = "changeme"
}

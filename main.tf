terraform {
  backend "s3" {
    bucket  = "productive-muslim-backend-dev" # change to productive muslim bucket
    key     = "pm-app.tfstate"                # path to file
    region  = "us-east-1"
    encrypt = true
    # dynamodb_table = "table-name"
  }
}

provider "aws" {
  region = var.aws-region
}

data "aws_region" "current" {}

locals {
  prefix = "${var.prefix}-${terraform.workspace}"
  common-tags = {
    Environment = terraform.workspace
    Project     = var.project
    Owner       = var.contact
    ManagedBy   = "Terraform"
  }
}

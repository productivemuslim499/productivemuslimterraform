data "aws_ami" "amazon_linux" {
  most_recent = true
  filter {
    name   = "name"
    values = ["amzn2-ami-hvm-2.0.*-x86_64-gp2"]
  }

  owners = ["amazon"]
}

# Bastion Role
resource "aws_iam_role" "bastion-role" {
  name               = "${local.prefix}-bastion-role"
  assume_role_policy = file("./templates/instance-profile-policy.json")

  tags = local.common-tags
}

# Attach policy to role
resource "aws_iam_role_policy_attachment" "bastion-attach-policy" {
  role       = aws_iam_role.bastion-role.name
  policy_arn = "arn:aws:iam::aws:policy/AmazonEC2ContainerRegistryReadOnly"
}

# iam profile
resource "aws_iam_instance_profile" "bastion-profile" {
  name = "${local.prefix}-bastion-profile"
  role = aws_iam_role.bastion-role.name
}

resource "aws_instance" "bastion" {
  ami                  = data.aws_ami.amazon_linux.id
  instance_type        = "t2.micro"
  user_data            = file("./templates/user-data.sh")
  iam_instance_profile = aws_iam_instance_profile.bastion-profile.name
  key_name             = var.key-name
  subnet_id            = aws_subnet.public-subnet[0].id

  vpc_security_group_ids = [aws_security_group.bastion-sg.id]
  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-bastion" })
  )
}

resource "aws_security_group" "bastion-sg" {
  description = "Control bastion inbound and outbound access"
  name        = "${local.prefix}-bastion"
  vpc_id      = aws_vpc.main.id

  # ssh 
  ingress {
    protocol    = "tcp"
    from_port   = 22
    to_port     = 22
    cidr_blocks = ["0.0.0.0/0"]
  }

  # https out
  egress {
    protocol    = "tcp"
    from_port   = 443
    to_port     = 443
    cidr_blocks = ["0.0.0.0/0"]
  }

  # standard http
  egress {
    protocol    = "tcp"
    from_port   = 80
    to_port     = 80
    cidr_blocks = ["0.0.0.0/0"]
  }

  egress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432
    cidr_blocks = [
      aws_subnet.private-subnet[0].cidr_block,
      aws_subnet.private-subnet[1].cidr_block,
    ]
  }

  tags = local.common-tags
}


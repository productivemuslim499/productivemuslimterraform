resource "aws_db_subnet_group" "db-main" {
  name = "${local.prefix}-main-rds-db"
  subnet_ids = [
    aws_subnet.private-subnet[0].id,
    aws_subnet.private-subnet[1].id,
  ]

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-database-subnet" })
  )
}

resource "aws_security_group" "rds-sg" {
  description = "Allow access to the RDS DB"
  name        = "${local.prefix}-rds-inbound-access"
  vpc_id      = aws_vpc.main.id

  ingress {
    protocol  = "tcp"
    from_port = 5432
    to_port   = 5432

    # linits ingress to the ec2 security group
    security_groups = [
      aws_security_group.bastion-sg.id
    ]
  }

  tags = local.common-tags
}

resource "aws_db_instance" "main-db" {
  identifier              = "${local.prefix}-db"
  db_name                 = "maindb"
  allocated_storage       = 20    # disk space
  storage_type            = "gp2" # entry level general purpose
  engine                  = "postgres"
  engine_version          = "13.1"
  instance_class          = "db.t2.micro"
  db_subnet_group_name    = aws_db_subnet_group.db-main.name
  password                = var.db-password
  username                = var.db-username
  backup_retention_period = 0 # backup days
  multi_az                = false
  skip_final_snapshot     = true
  vpc_security_group_ids  = [aws_security_group.rds-sg.id]

  tags = merge(
    local.common-tags,
    tomap({ "Name" = "${local.prefix}-main-db" })
  )
}
